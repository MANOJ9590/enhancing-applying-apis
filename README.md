# Enhancing-Applying-apis

Enhancement | Masterarbeit | Applying API Categories to the Abstractions Using APIs


# Notes
This is a Enhancement project of the master thesis "Applying API Categories to the Abstractions Using APIs" (2021) written by Katharina Gorjatschev as part of the MSR course 2021/22 at UniKo, CS department, SoftLang Team.


# Team Name: India
* Sriram Aralappa (219203195)
* Manojkumar Krishnakumar (220200906) 

# Baseline Study: 

### Aspect of the Enhancement project
We are collecting java repositories from Github and collecting dependencies which are eligible based on conditions given in the thesis paper for further process.Futher more enhancement was made in the collection of repositories independence of POM files as a key affecting factor to answer the Research Question :How could projects with multiple POM files impact dependency/usage analysis



### Input data:
Using Github API we were able to collect 1800 repositories with minmum of 2 POM files and then parsing these repositories files(CSV)  to find dependencies in Python.

### Output data:
comma seperated (CSV) files which contain all dependencies of repositories.
 
# Finding of Enhancement:
 
### Process delta: 
we collected all repositories which has more than two Pom file.Then we used first POM file for further process.This is done to check if we get any differences in dependencies.
###  Output delta: 
dependencies set counts differ from the original thesis  which answers our research question.


# Implementation of Enhancement: 

### Software
* Java 11 (Maven project)
* Python 3.9( pyspark==3.1.2)

### Data
We were able to produce csv files which contain Java repositories name and eligible dependencies file.

# Notes
You need to create a personal access token in your GitHub account and then replace the `USERNAME_AND_TOKEN` in `RepositoriesPicker.java` with your username and token.